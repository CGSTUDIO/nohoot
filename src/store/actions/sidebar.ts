export const ACTION_SET_OPEN_SIDEBAR = 'action/SET_OPEN_SIDEBAR';

export const actionSetOpenSidebar = (payload) => ({
  type: ACTION_SET_OPEN_SIDEBAR,
  payload,
});
