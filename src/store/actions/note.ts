export const ACTION_SET_EDIT_NOTE = 'action/ACTION_SET_EDIT_NOTE';

export const actionSetEditNote = (payload) => ({
  type: ACTION_SET_EDIT_NOTE,
  payload,
});
