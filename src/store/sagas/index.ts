import { delay, put, takeEvery } from 'redux-saga/effects';

const GET_ASYNC_REDUX_SAGA_PROP_TYPE = 'GET_ASYNC_REDUX_SAGA_PROP_TYPE';
const GET_ASYNC_REDUX_SAGA_PROP_TYPE_SUCCESS = 'GET_ASYNC_REDUX_SAGA_PROP_TYPE_SUCCESS';
const ASYNC_REDUX_SAGA_PROP_TEXT = 'ASYNC_REDUX_SAGA_PROP_TEXT';

const TEST = process.env.NODE_ENV === 'test';

function* getAsyncReduxSagaProp() {
  yield delay(TEST ? 100 : 2000);

  yield put({
    type: GET_ASYNC_REDUX_SAGA_PROP_TYPE_SUCCESS,
    data: ASYNC_REDUX_SAGA_PROP_TEXT,
  });
}

function* rootSaga() {
  yield takeEvery(GET_ASYNC_REDUX_SAGA_PROP_TYPE, getAsyncReduxSagaProp);
}

export default rootSaga;
