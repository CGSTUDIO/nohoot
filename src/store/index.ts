import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createWrapper } from 'next-redux-wrapper';
import rootReducer from '@/core/store/reducers/index';
import rootSaga from '@/core/store/sagas/index';

let store = null;

const bindMiddleware = (middleware) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension');
    return composeWithDevTools(applyMiddleware(middleware));
  }
  return applyMiddleware(middleware);
};

const makeStore: any = ({ isServer }) => {
  const sagaMiddleware = createSagaMiddleware();
  if (isServer) {
    // If it's on server side, create a store
    store = createStore(rootReducer, undefined, bindMiddleware(sagaMiddleware));
  } else {
    // If it's on client side, create a store which will persist
    const { persistStore, persistReducer } = require('redux-persist');
    const storage = require('redux-persist/lib/storage').default;

    const persistConfig = {
      key: 'root',
      // whitelist: ['counter'], // only counter will be persisted, add other reducers if needed
      storage, // if needed, use a safer storage
    };

    // Create a new reducer with our existing reducer
    const persistedReducer = persistReducer(persistConfig, rootReducer);

    store = createStore(persistedReducer, bindMiddleware(sagaMiddleware));

    // This creates a persistor object & push that persisted object
    // to .__persistor, so that we can avail the persistability feature
    store.__persistor = persistStore(store);
  }
  store.sagaTask = sagaMiddleware.run(rootSaga);
  return store;
};

const wrapper = createWrapper(makeStore);
export default wrapper;
