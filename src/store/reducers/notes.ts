import { Card } from '@/core/types';
import { produce } from 'immer';
import { ACTION_SET_EDIT_NOTE } from '../actions/note';

const initialState = {
  item: {},
};

const notes = (state = initialState, action: any = {}) =>
  produce(state, (draft) => {
    console.log(action.payload);
    switch (action.type) {
      case ACTION_SET_EDIT_NOTE:
        draft.item = action.payload.item;
        break;
      default:
        return state;
    }
  });

export default notes;
