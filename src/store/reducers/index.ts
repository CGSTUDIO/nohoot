import { combineReducers } from 'redux';
import sidebar from './sidebar';
import notes from './notes';

const rootReducer = combineReducers({ sidebar, notes });

export default rootReducer;
