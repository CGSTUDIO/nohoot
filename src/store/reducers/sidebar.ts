import { produce } from 'immer';
import { ACTION_SET_OPEN_SIDEBAR } from '../actions/sidebar';

const initialState = {
  right: false,
  left: true,
};

const authentication = (state = initialState, action: any = {}) =>
  produce(state, (draft) => {
    console.log(action.payload);
    switch (action.type) {
      case ACTION_SET_OPEN_SIDEBAR:
        draft[action.payload.sidebar] = action.payload.open;
        break;
      default:
        return state;
    }
  });

export default authentication;
