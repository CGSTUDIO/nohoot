import { toDate } from '@/core/utils/date';
import { useEditor } from '@tiptap/react';
import StarterKit from '@tiptap/starter-kit';
import clsx from 'clsx';
import { useState } from 'react';
import { RootStateOrAny, useSelector } from 'react-redux';
import MenuEditor from '../../atoms/TextEditor/MenuEditor';
import TextEditor from '../../atoms/TextEditor/TextEditor';

const SidebarRight = () => {
  const open = useSelector((state: RootStateOrAny) => state.sidebar.right);
  const item = useSelector((state: RootStateOrAny) => state.notes.item);
  const [showTextEditor, setShowTextEditor] = useState<boolean>(false);

  const editor = useEditor({
    extensions: [StarterKit],
    content: `<p>${item.paragraph}</p>`,
  });

  return (
    <section
      className={clsx('z-50 h-screen fixed bg-black right-0 transition-all duration-700', open ? 'w-700' : 'w-0')}
      onClick={() => setShowTextEditor(false)}
    >
      <header>
        <h2>{item.author}</h2>
        <p>{toDate(item.date)}</p>
      </header>
      <article>
        <h4>{item.title}</h4>
        <section
          onClick={(e) => {
            e.stopPropagation();
            setShowTextEditor(true);
          }}
        >
          {showTextEditor ? <TextEditor editor={editor} /> : <p>{item.paragraph}</p>}
        </section>
      </article>
    </section>
  );
};

export default SidebarRight;
