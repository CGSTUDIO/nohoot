import { actionSetEditNote } from '@/core/store/actions/note';
import { actionSetOpenSidebar } from '@/core/store/actions/sidebar';
import { Card } from '@/core/types';
import { useCallback } from 'react';
import { IoAddCircleOutline } from 'react-icons/io5';
import { DefaultRootState, RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import CardNote from '../../molecules/CardNote/CardNote';

type Props = {
  items: Card[];
};

const CardList = ({ items }: Props) => {
  const sidebarRightOpen = useSelector((state: RootStateOrAny) => state.sidebar.right);
  const dispatch = useDispatch();
  const onClick = useCallback(
    (item) => {
      dispatch(actionSetEditNote({ item }));
      dispatch(actionSetOpenSidebar({ sidebar: 'right', open: true }));
    },
    [actionSetEditNote, dispatch],
  );
  return (
    <section className="z-50 grid grid-flow-row grid-cols-4 grid-rows-4 gap-12 m-auto">
      <CardNote.Children>
        <section className="flex flex-col items-center justify-center justify-items-center">
          <IoAddCircleOutline color="#C0C0C0" size={25} />
          <p className="text-xs text-lightgrey font-light mt-2">Ajouter une note</p>
        </section>
      </CardNote.Children>
      {items.map((item) => (
        <CardNote.Item key={item.title} item={item} onClick={() => onClick(item)} />
      ))}
    </section>
  );
};

export default CardList;
