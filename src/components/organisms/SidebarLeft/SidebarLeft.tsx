import menu from '@/core/constants/menu';
import { ItemNavigation } from '@/core/types';
import { useDispatch } from 'react-redux';

const SidebarLeft = () => {
  const dispatch = useDispatch();
  return (
    <section className="h-screen fixed z-50">
      {menu.map((item: ItemNavigation, index) => {
        const Icon = item.icon;
        return (
          <section
            key={item.name + index}
            className="flex items-center justify-center justify-items-center
          h-14 w-14 rounded-lg bg-black bg-opacity-20 m-8 hover:bg-white hover:bg-opacity-20
          transition-all duration-500 cursor-pointer"
            onClick={() => dispatch(item.action({ sidebar: 'right', open: true }))}
          >
            <Icon size={30} color="#fff" />
          </section>
        );
      })}
    </section>
  );
};

export default SidebarLeft;
