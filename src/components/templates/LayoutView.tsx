import { FC } from 'react';
import SidebarLeft from '../organisms/SidebarLeft/SidebarLeft';
import SidebarRight from '../organisms/SidebarRight/SidebarRight';

const LayoutView: FC = ({ children }) => (
  <section id="container" className="flex w-full h-screen overflow-y-hidden">
    <SidebarLeft />
    <section className="w-full my-20">{children}</section>
    <SidebarRight />
  </section>
);

export default LayoutView;
