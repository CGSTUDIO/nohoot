import Image from 'next/image';

type Props = {
  src?: string;
};

const style = {
  width: 40,
  height: 40,
  borderRadius: 50,
  background: 'grey',
};

const Avatar = ({ src }: Props) => {
  return src ? <Image width={40} height={40} src={src} className="rounded-full" /> : <section style={style}></section>;
};

export default Avatar;
