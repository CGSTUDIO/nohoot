import { useEditor, EditorContent } from '@tiptap/react';
import StarterKit from '@tiptap/starter-kit';
import { RootStateOrAny, useSelector } from 'react-redux';
import MenuEditor from './MenuEditor';

const TextEditor = ({ editor }) => {
  return (
    <section className="flex flex-col">
      <MenuEditor editor={editor} />
      <EditorContent editor={editor} />;
    </section>
  );
};

export default TextEditor;
