import { BiBold, BiCodeAlt, BiStrikethrough, BiItalic } from 'react-icons/bi';
import { MdClear, MdFormatListBulleted } from 'react-icons/md';
import clsx from 'clsx';

type Props = {
  editor: any;
};

// {
//   name: 'code',
//   icon: BiCodeAlt,
//   action: editor.chain().focus().toggleCode().run,
// },
// {
//   icon: MdClear,
//   action: editor.chain().focus().clearNodes().run.run,
// },
// {
//   name: 'bulletList',
//   icon: MdFormatListBulleted,
//   action: editor.chain().focus().toggleBulletList().run,
// },

const MenuEditor = ({ editor }: Props) => {
  if (!editor) return null;
  return (
    <>
      <button
        onClick={() => editor.chain().focus().toggleBold().run()}
        className={editor.isActive('bold') ? 'is-active' : ''}
      >
        <BiBold color="#fff" />
      </button>
      <button
        onClick={() => editor.chain().focus().toggleItalic().run()}
        className={editor.isActive('italic') ? 'is-active' : ''}
      >
        <BiItalic color="#fff" />
      </button>
      <button
        onClick={() => editor.chain().focus().toggleStrike().run()}
        className={editor.isActive('strike') ? 'is-active' : ''}
      >
        <BiStrikethrough color="#fff" />
      </button>
      {/* {generateMenu(editor).map((item, index) => {
        const Icon = item.icon;
        return (
          <button
            key={item.name ?? index}
            onClick={item.action}
            className={clsx('m-4', editor.isActive(item.name) ? 'is-active' : '')}
          >
            <Icon size={20} color="#fff" />
          </button>
        );
      })} */}
    </>
  );
};

export default MenuEditor;
