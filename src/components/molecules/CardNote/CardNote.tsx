import { Card } from '@/core/types';
import Avatar from '../../atoms/Avatar/Avatar';
import { MouseEventHandler } from 'react';
import { toDate } from '@/core/utils/date';

type PropsComponentItem = {
  item: Card;
  onClick: MouseEventHandler<HTMLElement>;
};

type PropsComponentWithChildren = {
  children: JSX.Element;
};

const CardNote = {
  Item: ({ item, onClick }: PropsComponentItem) => (
    <section
      onClick={onClick}
      className="w-60 h-80 bg-white bg-opacity-30 rounded-lg
     bg-clip-padding backdrop-filter backdrop-blur-md shadow-lg cursor-pointer"
    >
      <header className="flex m-4">
        <Avatar src={item.avatar} />
        <section>
          <p className="text-sm mx-4">{item.author}</p>
          <p className="text-xs mx-4">{toDate(item.date)}</p>
        </section>
      </header>
      <div className="h-px w-full bg-black bg-opacity-20 my-4" />
      <article className="m-8">
        <h3 className="text-left text-lg text-black text-opacity-80">{item.title}</h3>
        <p className="text-md text-black text-opacity-50 mt-4">{item.paragraph.slice(0, 80).concat('...')}</p>
      </article>
    </section>
  ),
  Children: ({ children }: PropsComponentWithChildren) => (
    <section
      className="flex justify-center w-60 h-80 bg-black bg-opacity-30
       hover:bg-white hover:bg-opacity-10 rounded-lg transition-all duration-500
        bg-clip-padding backdrop-filter backdrop-blur-md shadow-lg cursor-pointer"
    >
      {children}
    </section>
  ),
};

export default CardNote;
