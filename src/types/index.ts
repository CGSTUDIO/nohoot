import { ReactNode } from 'react';
export interface Card {
  author: string;
  title: string;
  paragraph: string;
  date: Date;
  avatar?: string;
}

interface ItemActionNavigation {
  sidebar: 'right' | 'left';
  open: boolean;
}

export interface ItemNavigation {
  name: string;
  icon: any;
  action?: (arg: ItemActionNavigation) => void;
}
