const colors = {
  black: '#2c2c2e',
  lightgrey: '#ccc',
};

module.exports = { colors };
