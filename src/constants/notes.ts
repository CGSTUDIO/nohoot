export default [
  {
    author: 'Juanita Doe',
    avatar: '/images/user.jpeg',
    title: 'Hello world 1',
    paragraph:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    date: new Date(),
  },
  {
    author: 'Juanita Doe',
    avatar: '/images/user.jpeg',
    title: 'Hello world 2',
    paragraph:
      'when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
    date: new Date(),
  },
  {
    author: 'Juanita Doe',
    avatar: '/images/user.jpeg',
    title: 'Hello world 3',
    paragraph:
      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
    date: new Date(),
  },
  {
    author: 'Juanita Doe',
    avatar: '/images/user.jpeg',
    title: 'Hello world 4',
    paragraph: "Ma petit list d'idée",
    date: new Date(),
  },
  {
    author: 'Juanita Doe',
    avatar: '/images/user.jpeg',
    title: 'Hello world 5',
    paragraph: "Ma petit list d'idée",
    date: new Date(),
  },
];
