import { CgFormatSlash } from 'react-icons/cg';
import { IoMdAdd, IoIosList } from 'react-icons/io';
import { actionSetOpenSidebar } from '../store/actions/sidebar';

export default [
  {
    name: 'Spotlight',
    icon: CgFormatSlash,
    action: actionSetOpenSidebar,
  },
  {
    name: 'Ajouter une note',
    icon: IoMdAdd,
  },
  {
    name: 'Mes notes',
    icon: IoIosList,
  },
];
