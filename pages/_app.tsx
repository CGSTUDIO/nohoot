import '@/core/styles/globals.css';
import '@/core/styles/tiptap.css';

import { useStore, Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import withReduxSaga from 'next-redux-saga';
import wrapper from '@/core/store/index';
import LayoutView from '@/core/components/templates/LayoutView';
import * as dayjs from 'dayjs';
import isLeapYear from 'dayjs/plugin/isLeapYear'; // import plugin
import 'dayjs/locale/fr'; // import locale

dayjs.extend(isLeapYear); // use plugin
dayjs.locale('fr');

function App({ Component, pageProps }) {
  const store: any = useStore();
  return (
    <Provider store={store}>
      <PersistGate persistor={store.__persistor} loading={<div>Loading</div>}>
        <LayoutView>
          <Component {...pageProps} />
        </LayoutView>
      </PersistGate>
    </Provider>
  );
}

export default wrapper.withRedux(withReduxSaga(App));
