import { NextSeo } from 'next-seo';
import ProjectsView from '@/core/components/templates/ProjectsView';

const Projects = () => (
  <>
    <NextSeo noindex nofollow />
    <ProjectsView />
  </>
);

export default Projects;
