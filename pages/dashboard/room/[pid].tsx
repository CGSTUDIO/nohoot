import RoomView from '@/core/components/templates/RoomView';
import StudentRoom from '@/core/components/organisms/Room/StudentRoom';
import RoomHeader from '@/core/components/molecules/RoomHeader/RoomHeader';
import TeacherRoom from '@/core/components/organisms/Room/TeacherRoom';

const Room = () => {
  const isTeacher = true;
  return (
    <RoomView>
      <RoomHeader />
      {!isTeacher ? <StudentRoom /> : <TeacherRoom />}
    </RoomView>
  );
};

export default Room;
