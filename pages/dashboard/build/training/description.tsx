import { useState } from 'react';
import * as yup from 'yup';
import Button from '@/core/components/atoms/Button/Button';
import { BsArrowRight } from 'react-icons/bs';

import Description from '@/core/components/organisms/Description/Description';
import ToasterStep from '@/core/components/molecules/ToasterStep/ToasterStep';

const validationSchema = yup.object().shape({
  title: yup
    .string()
    .required('Veuillez renseigner un titre')
    .min(10, 'Votre titre doit contenir un minimum de 10 caractères'),
  description: yup
    .string()
    .required('Veuillez renseigner une description')
    .min(10, 'Votre description doit contenir un minimum de 10 caractères'),
});

const Training = () => {
  /* eslint-disable-next-line */
  const [htmlValue, setHtmlValue] = useState('');
  return (
    <section className="flex h-full">
      <ToasterStep />
      <section className="w-2/4 m-auto">
        <Description
          classContainer="w-full"
          labelTitleInput="Titre"
          nameTitleInput="titre"
          labelTextarea="Description"
          nameTextarea="description"
          validationSchema={validationSchema}
          htmlValue={htmlValue}
          setHtmlValue={setHtmlValue}
          submitButton={
            <section className="absolute bottom-10 right-20">
              <Button type="button" behavior="block" borderShape="circle">
                <BsArrowRight color="white" size={30} />
              </Button>
            </section>
          }
        />
      </section>
    </section>
  );
};

export default Training;
