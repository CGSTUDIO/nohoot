import { useState, forwardRef } from 'react';
import clsx from 'clsx';
import DatePicker, { registerLocale } from 'react-datepicker';
import { BiChevronRight } from 'react-icons/bi';
import fr from 'date-fns/locale/fr';
import Button from '@/core/components/atoms/Button/Button';
import { BsArrowRight } from 'react-icons/bs';
import ToasterStep from '@/core/components/molecules/ToasterStep/ToasterStep';
// the locale you want
registerLocale('fr', fr); // register it with the name you want

// for dynamic local date
// https://stackoverflow.com/questions/54399084/change-locale-in-react-datepicker

const ButtonDatePicker = forwardRef(({
 value, onClick, className, placeholder,
}: any, ref: any) => (
  <button
    className={clsx(
      'flex p-3 pl-6 rounded-lg w-full border border-blue font-light items-center',
      'bg-white',
      value ? 'text-blue' : 'text-lightgrey',
      className,
    )}
    onClick={onClick}
    ref={ref}
  >
    {value || placeholder}
    <div className="m-auto flex bg-blue rounded w-8 h-8 mr-4">
      <BiChevronRight className="m-auto" color="white" size={25} />
    </div>
  </button>
));

const Training = () => {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  return (
    <section className="flex h-full">
      <ToasterStep />
      <section className="w-2/4 m-auto">
        <DatePicker
          selected={startDate ?? null}
          minDate={new Date()}
          locale="fr"
          dateFormat="eeee d MMMM yyyy"
          onChange={(date) => setStartDate(date)}
          selectsStart
          startDate={startDate}
          endDate={endDate}
          placeholderText="Sélectionner une date de commencement"
          className="mb-4"
          customInput={<ButtonDatePicker />}
        />
        <DatePicker
          selected={endDate ?? null}
          locale="fr"
          dateFormat="eeee d MMMM yyyy"
          onChange={(date) => setEndDate(date)}
          selectsEnd
          startDate={startDate}
          endDate={endDate}
          minDate={startDate}
          placeholderText="Sélectionner une date de fin"
          customInput={<ButtonDatePicker />}
        />
      </section>
      <section className="absolute bottom-10 right-20">
        <Button type="button" behavior="block" borderShape="circle">
          <BsArrowRight color="white" size={30} />
        </Button>
      </section>
    </section>
  );
};

export default Training;
