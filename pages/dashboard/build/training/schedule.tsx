import { useState, useCallback, createRef } from 'react';
import {
 Formik, Form, Field, FormikHelpers,
} from 'formik';
import * as yup from 'yup';
/* eslint-disable-next-line */
import ListChapterEditable from '@/core/components/organisms/ListChapterEditable/ListChapterEditable';
import TextInputField from '@/core/components/atoms/TextInputField/TextInputField';
import Button from '@/core/components/atoms/Button/Button';
import { ValuesFormBuildChapitre, chapitreType } from '@/core/types/formBuildTraining';
import { BsArrowRight } from 'react-icons/bs';
import { IoIosAdd } from 'react-icons/io';
import ToasterStep from '@/core/components/molecules/ToasterStep/ToasterStep';

const validationSchema = yup.object().shape({
  entitled: yup
    .string()
    .required('Veuillez renseigner un titre')
    .min(10, 'Votre titre doit contenir un minimum de 10 caractères'),
});

const Schedule = () => {
  const [chaptersList, setChaptersList] = useState<chapitreType[]>([]);
  const [editChapter, setEditChapter] = useState<any>(null);

  const onSubmitAddChapter = useCallback(
    (chapterList) => {
      const newList = { ...chapterList, resources: null, ref: createRef() };
      setChaptersList((prevChapitreList) => [newList, ...prevChapitreList]);
    },
    [setChaptersList],
  );

  return (
    <section className="flex h-full flex-col">
      <ToasterStep />
      <section className="w-2/4">
        <Formik
          validationSchema={validationSchema}
          initialValues={{
            entitled: '',
          }}
          onSubmit={(
            values,
            { setSubmitting, resetForm }: FormikHelpers<ValuesFormBuildChapitre>,
          ) => {
            onSubmitAddChapter(values);
            resetForm();
            setSubmitting(false);
          }}
        >
          <Form>
            <section className="relative">
              <Field
                type="text"
                label="Titre du chapitre"
                name="entitled"
                inputColor="bg-white"
                component={TextInputField}
                insideNode={
                  <Button type="submit">
                    <IoIosAdd />
                  </Button>
                }
              />
            </section>
          </Form>
        </Formik>
      </section>
      <section className="w-2/4 mt-16 overflow-auto">
        <ListChapterEditable
          chaptersList={chaptersList}
          setChaptersList={setChaptersList}
          editChapter={editChapter}
          setEditChapter={setEditChapter}
        />
      </section>
      <section className="absolute bottom-10 right-20">
        <Button type="button" behavior="block" borderShape="circle">
          <BsArrowRight color="white" size={30} />
        </Button>
      </section>
    </section>
  );
};

export default Schedule;
