import { useState } from 'react';
import { sanitize } from 'dompurify';
import * as yup from 'yup';
import Description from '@/core/components/organisms/Description/Description';
import ToasterStep from '@/core/components/molecules/ToasterStep/ToasterStep';

const validationSchema = yup.object().shape({
  title: yup
    .string()
    .required('Veuillez renseigner un titre')
    .min(3, 'Votre titre doit contenir un minimum de 10 caractères'),
  note: yup
    .string()
    .required('Veuillez renseigner une description')
    .min(10, 'Votre description doit contenir un minimum de 10 caractères'),
});

const Note = () => {
  const [htmlValue, setHtmlValue] = useState<string>('');
  const [inputValue, setInputValue] = useState<string>('');

  return (
    <section className="flex">
      <ToasterStep />
      <section className="w-2/4">
        <Description
          setInputValue={setInputValue}
          classContainer="w-full"
          htmlValue={htmlValue}
          labelTitleInput="Titre de la note écrite"
          nameTitleInput="title"
          labelTextarea="Note écrite"
          nameTextarea="note"
          validationSchema={validationSchema}
          setHtmlValue={setHtmlValue}
        />
      </section>
      <section className="w-2/4 h-96 overflow-auto p-14 pl-24">
        <section className="text-3xl">{inputValue}</section>
        <section className="pt-10" dangerouslySetInnerHTML={{ __html: sanitize(htmlValue) }} />
      </section>
    </section>
  );
};

export default Note;
