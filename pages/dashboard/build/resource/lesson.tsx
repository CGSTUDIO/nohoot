import { useState } from 'react';
import * as yup from 'yup';
import ToasterStep from '@/core/components/molecules/ToasterStep/ToasterStep';
import Button from '@/core/components/atoms/Button/Button';
import { IoIosAdd } from 'react-icons/io';
import { Formik, Form, Field } from 'formik';
import TextInputField from '@/core/components/atoms/TextInputField/TextInputField';
/* eslint-disable-next-line */
import TextareaEditorFormik from '@/core/components/atoms/TextareaEditorFormik/TextareaEditorFormik';
import ListSentences from '@/core/components/organisms/ListSentences/ListSentences';

const validationTitleSchema = yup.object().shape({
  title: yup
    .string()
    .required('Veuillez renseigner un titre')
    .min(10, 'Votre titre doit contenir un minimum de 10 caractères'),
});

const validationSentenceSchema = yup.object().shape({
  sentence: yup.string().required('Veuillez remplir ce champs'),
});

const Lesson = () => {
  const [listSentences, setListSentences] = useState<any>([]);
  const [htmlValue, setHtmlValue] = useState<string>('');
  /* eslint-disable-next-line */
  const [inputValue, setInputValue] = useState<string>('');

  return (
    <section className="flex">
      <ToasterStep />
      <section className="w-2/4 relative">
        <Formik
          validationSchema={validationTitleSchema}
          initialValues={{
            title: '',
          }}
          onSubmit={(_values, { setSubmitting }) => {
            setSubmitting(false);
          }}
        >
          <Form>
            <Field
              type="text"
              label="Titre du cours"
              name="title"
              inputColor="bg-white"
              setInputValue={setInputValue}
              component={TextInputField}
            />
          </Form>
        </Formik>

        <Formik
          validationSchema={validationSentenceSchema}
          initialValues={{
            sentence: '',
          }}
          onSubmit={(_values, { setSubmitting }) => {
            setListSentences((prevState) => [...prevState, htmlValue]);
            setSubmitting(false);
          }}
        >
          {({ errors }) => (
            <Form>
              <section className="h-100 mt-5 relative">
                <Field
                  type="text"
                  label="Ecrire une phrase par une phrase"
                  name="sentence"
                  className="h-24"
                  inputColor="bg-white"
                  component={TextareaEditorFormik}
                  setHtmlValue={setHtmlValue}
                />
                {!errors.sentence && (
                  <section className="absolute -right-2 -bottom-14">
                    <Button padding="p-0" type="submit" behavior="block" borderShape="circle">
                      <IoIosAdd color="white" size={20} />
                    </Button>
                  </section>
                )}
              </section>
            </Form>
          )}
        </Formik>
      </section>
      <section className="w-2/4 ml-14 overflow-auto h-screen-85">
        <ListSentences list={listSentences} setList={setListSentences} />
      </section>
    </section>
  );
};

export default Lesson;
