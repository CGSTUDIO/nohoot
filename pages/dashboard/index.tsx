import { NextSeo } from 'next-seo';
import DashboardView from '@/core/components/templates/DashboardView';

const Dashboard = () => (
  <>
    <NextSeo noindex nofollow />
    <DashboardView />
  </>
);

export default Dashboard;
