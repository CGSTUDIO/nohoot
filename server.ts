// @ts-ignore
const cors = require('cors');
const express = require('express');
const next = require('next');
// const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const { createProxyMiddleware } = require('http-proxy-middleware');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

// const corsOptions = {
//   origin: ['http://localhost:3000'],
//   methods: ['GET', 'POST', 'DELETE', 'PATCH', 'UPDATE'],
//   credentials: true, // enable set cookie
// };

// extract to .env
// const secretToken = 'jgdfhdjk35GHH43tg54654FGE45Yh!%+esrt!45ERGF';

const apiPaths = {
  beta: {
    target: 'http://localhost:5000',
    pathRewrite: {
      '^/api/beta': '/api/beta',
    },
    changeOrigin: true,
  },
};
app.prepare().then(() => {
  const server = express();
  server.use(cors());
  server.use(express.json());
  server.use(express.urlencoded({ extended: false }));
  server.use(cookieParser());

  server.use('/api/beta', createProxyMiddleware(apiPaths.beta));

  server.all('*', (req, res) => handle(req, res));

  server.listen(3008, (err) => {
    if (err) throw err;
  });
});
