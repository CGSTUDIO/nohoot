// tailwind.config.js
const { colors } = require('./src/styles/theme/colors');

// https://tailwindcss.com/docs/adding-new-utilities
module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      extend: {
        backgroundImage: () => ({
          'hero-pattern': "url('~/images/bg.jpeg')",
        }),
      },
      fontSize: {
        xs: '.70rem',
      },
      colors: {
        ...colors,
      },
      textColor: {
        ...colors,
      },
      borderColor: {
        ...colors,
      },
      backgroundColor: {
        ...colors,
      },
      width: {
        300: '300px',
        500: '500px',
        700: '700px',
        '5%': '5%',
        '10%': '10%',
        '15%': '15%',
        '20%': '20%',
        '25%': '25%',
        '45%': '45%',
        '60%': '60%',
        '70%': '70%',
        '75%': '75%',
        '85%': '85%',
        '90%': '90%',
      },
      margin: {
        '3-5%': '3.5%',
        '5%': '5%',
        '25%': '25%',
        '30%': '30%',
      },
      height: {
        'screen-10': '10vh',
        'screen-15': '15vh',
        'screen-25': '25vh',
        'screen-45': '45vh',
        'screen-49': '49vh',
        'screen-50': '50vh',
        'screen-70': '70vh',
        'screen-75': '75vh',
        'screen-80': '80vh',
        'screen-85': '85vh',
        'screen-90': '90vh',
        'screen/2': '50vh',
        'screen/3': 'calc(100vh / 3)',
        'screen/4': 'calc(100vh / 4)',
        'screen/5': 'calc(100vh / 5)',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
